#!/bin/bash

# Get Root Permission
su

# Install required packages
pacman -S --needed fish kitty neovim bluez bluez-utils pulseaudio-bluetooth

# Change default shell to Fish
if [ "$SHELL" != "/usr/bin/fish" ]; then chsh -s /usr/bin/fish && exec /usr/bin/fish; fi

# Enable Blutooth
systemctl enable bluetooth.service

# Apply Fish configuration
mkdir -p ~/.config/fish
cp .config/fish/config.fish ~/.config/fish/

# Apply Kitty configuration
mkdir -p ~/.config/kitty
cp .config/kitty/kitty.conf ~/.config/kitty/

# Apply Neovim configuration
git clone https://github.com/NvChad/NvChad ~/.config/nvim --depth 1

# Apply rxfetch binary
cp bin/rxfetch /usr/bin/

# Apply wallpaper images
cp assets/wallpapers/* /usr/share/wallpapers/

# Copy hooks
mkdir -p /etc/pacman.d/hooks/
cp hooks/* /etc/pacman.d/hooks/

