<div style="text-align:center">
<img src="./assets/Dotfiles.png">
</div>

# **KDE**

![KDE Screenshot](./assets/greatwave.jpeg)

![neofetch Screenshot](./assets/term.png)

**OS** : Arch Linux

**Shell** : Fish

**DE** : KDE Plasma

**Theme** : Arch

**Icons** : Zafiro-Icons-Light-Blue

**Terminal** : kitty

**Editor** : nvim(NvChad)

**Editor-Theme** : tokyodark

# **Nvim**

![Nvim Greet](./assets/nvimgreet.png)
![Nvim](./assets/nvim.png)

## Contents

- kitty Config
- fish Config
- rxfetch Config
- zsh Config
- nvim Config

## Me

- [Other Projects](https://gitlab.com/users/hariz_ms/projects)
- [My Todo app](https://doitrightnow.netlify.app/)
