rxfetch

bind \cf ff

set -e fish_user_paths
set -U fish_user_paths $HOME/.local/bin $HOME/Applications $fish_user_paths

#Supresses fish's intro message
set fish_greeting
set LANG en_US.UTF-8

#bat as anpager
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

#Default Applications
set EDITOR nvim

#Alias
alias lla 'lsd -alh' 
alias ls 'lsd -h'
alias la 'lsd -A'
alias cp "cp -iv"
alias mv 'mv -iv'
alias rm 'rm -vI'
alias mkdir 'mkdir -pv'
alias sn 'shutdown now'
alias cleanup 'sudo pacman -Rcns (pacman -Qtdq)'
alias rfe 'shred -un25'
alias pacf 'pacman -Slq | fzf --multi --preview "pacman -Si {1}" | xargs -ro sudo pacman -S'
alias pacr 'pacman -Qeqt | fzf --multi --preview "pacman -Qi {1}" | xargs -ro sudo pacman -Rns'
alias aurf='paru -Slq --aur | fzf --multi --preview "paru -Si {1}" | xargs -ro paru -S'
alias stats 'git status'
alias add 'git add'
alias commit 'git commit -m'
